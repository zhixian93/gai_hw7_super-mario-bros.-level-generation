package dk.itu.mario.engine.level;

import java.util.Random;
import java.text.DecimalFormat;
import java.util.*;

//Make any new member variables and functions you deem necessary.
//Make new constructors if necessary
//You must implement mutate() and crossover()


public class MyDNA extends DNA
{
	
	public int numGenes = 0; //number of genes

	// Return a new DNA that differs from this one in a small way.
	// Do not change this DNA by side effect; copy it, change the copy, and return the copy.
	public MyDNA mutate ()
	{
		MyDNA copy = new MyDNA();
		//YOUR CODE GOES BELOW HERE
		double possChange = 0.3;
		String geno = this.getChromosome();
		String[] blockArray = geno.split("\\}, \\{");
		int[] blockHeight = new int[22];
		int[] blockType = new int[22];
		int[] blockDifficulty = new int[22];
		blockHeight[0] = 13;
		blockHeight[21] = 13;
		for (int i = 1; i < blockArray.length - 1; i++) {
			String block = blockArray[i];
			String[] featureArray = block.split(", ");
			HashMap<String, Integer> map = new HashMap<>();
			for (String feature: featureArray) {
				String[] temp = feature.split(": ");
				map.put(temp[0], Integer.parseInt(temp[1]));
			}
			blockHeight[i] = map.get("height");
			blockType[i] = map.get("type");
			blockDifficulty[i] = map.get("difficulty");
		}
		//System.out.println("mutation:");
		//System.out.println("origin:\n " + this.getChromosome());
		String out = "{level: 00, height: 13}";
		for (int i = 1; i <= 20; i++) {
			Random random = new Random();
			DecimalFormat fmt = new DecimalFormat("00");
			String outpart = ", {level: " + fmt.format(i);
			if (random.nextDouble() < 0.2) {
				int lastHeight = blockHeight[i - 1];
				int nextHeight = blockHeight[i + 1];
				int height;
				if (nextHeight < lastHeight) height = Math.min(nextHeight + 2, Math.max(lastHeight - 2, nextHeight + random.nextInt(lastHeight - nextHeight)));
				else {
					int tempHeight = 14 - random.nextInt(14 - lastHeight + 3);
		        	height = Math.max(tempHeight, 1);
				}
				blockHeight[i] = height;
				outpart = outpart + ", height: " + fmt.format(height);
			}
			else {
				outpart = outpart + ", height: " + fmt.format(blockHeight[i]);
			}
			if (random.nextDouble() < 0.2) {
				int tempType = random.nextInt(5);
				blockType[i] = tempType;
	        	outpart = outpart + ", type: " + tempType;
			}
			else {
				outpart = outpart + ", type: " + blockType[i];
			}
			if (random.nextDouble() < 0.2) {
				int tempDifficulty = random.nextInt(40);
				blockDifficulty[i] = tempDifficulty;
	        	outpart = outpart + ", difficulty: " + fmt.format(tempDifficulty);
			}
			else {
				outpart = outpart + ", difficulty: " + fmt.format(blockDifficulty[i]);
			}
			outpart = outpart + "}";
			out = out + outpart;
		}
		out = out + ", {level: 21, height: 13}";
		copy.setChromosome(out);
		//System.out.println("mutated:\n " + copy.chromosome);
		//YOUR CODE GOES ABOVE HERE
		return copy;
	}
	
	// Do not change this DNA by side effect
	public ArrayList<MyDNA> crossover (MyDNA mate)
	{
		ArrayList<MyDNA> offspring = new ArrayList<MyDNA>();
		//YOUR CODE GOES BELOW HERE
        String myself = this.getChromosome();
        System.out.println("myselflength = " + myself.length());
        String wife = mate.getChromosome();
        System.out.println("wifelength = " + wife.length());
        int i = 0;
        int start = -1;
        while (i < myself.length()) {
        	if (start != -1 && myself.charAt(i) == wife.charAt(i)) {
        		String myselfcut = myself.substring(start, i);
        		String wifecut = wife.substring(start, i);
        		String myself2 = myself.substring(0, start) + wifecut + myself.substring(i);
        		String wife2 = wife.substring(0, start) + myselfcut + wife.substring(i);
        		if (myself.substring(start - 8, start - 2).equals("height") || myself.substring(i+2, i + 6).equals("type")) {
        			start = -1;
        			i++;
        			continue;
        		}
        		MyDNA arata1 = new MyDNA();
        		MyDNA arata2 = new MyDNA();
        		arata1.setChromosome(myself2);
        		arata2.setChromosome(wife2);
        		offspring.add(arata1);
        		offspring.add(arata2);
        		start = -1;
        	}
        	if (myself.charAt(i) == wife.charAt(i)) {
        		i++;
        		continue;
        	}
        	start = i;
        	i++;
        }
		//YOUR CODE GOES ABOVE HERE
		return offspring;
	}
	
	// Optional, modify this function if you use a means of calculating fitness other than using the fitness member variable.
	// Return 0 if this object has the same fitness as other.
	// Return -1 if this object has lower fitness than other.
	// Return +1 if this objet has greater fitness than other.
	public int compareTo(MyDNA other)
	{
		int result = super.compareTo(other);
		//YOUR CODE GOES BELOW HERE
		
		//YOUR CODE GOES ABOVE HERE
		return result;
	}
	
	
	// For debugging purposes (optional)
	public String toString ()
	{
		String s = super.toString();
		//YOUR CODE GOES BELOW HERE
		
		//YOUR CODE GOES ABOVE HERE
		return s;
	}
	
	public void setNumGenes (int n)
	{
		this.numGenes = n;
	}

}

